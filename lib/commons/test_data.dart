// Copyright (c) 2022 CHANGLEI. All rights reserved.

/// Created by changlei on 2022/1/19.
///
/// 测试数据

/// 头像
const avatar = 'https://img0.baidu.com/it/u=1056811702,4111096278&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500';

/// 封面
const cover = 'https://img.zcool.cn/community/01b3175ebd2e20a8012148144e9925.png@1280w_1l_2o_100sh.png';

/// 直播背景
const background =
    'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202004%2F16%2F20200416174631_lehva.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1645679402&t=936edf2426425c9868738fb17ee79f64';

/// 背景音乐
const backgroundMusic =
    'https://sdk-liteav-1252463788.cos.ap-hongkong.myqcloud.com/app/res/bgm/trtc/PositiveHappyAdvertising.mp3';

/// 直播占位图
const placeholderImage = 'https://st-gdx.dancf.com/gaodingx/391/articles/0/20200527-145342-0a6e.png';
